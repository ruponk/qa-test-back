import type { Request, Response, NextFunction } from 'express';

export class HttpError extends Error {
  httpCode: number;
  data: any;

  constructor({ message, httpCode = 500, data = {} }: { message: string; httpCode?: number, data?: any }) {
    super(message);

    this.httpCode = httpCode;
    this.data = data;
  }
}

export function errorMiddleware<T extends Error = any>(error: T, req: Request, res: Response, next: NextFunction) {
  const { message, stack } = error;
  let httpCode = 500, data = {};

  if (error instanceof HttpError) {
    httpCode = error.httpCode;
    data = error.data;
  }

  const { method, path } = req;
  console.error(`error middleware received error`, { method, path, message, stack, data });

  res.status(httpCode).json({ message, stack, data });
}
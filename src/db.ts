import sqlite3 from 'sqlite3';

export class Database {
  private _db: sqlite3.Database;

  constructor() {
    this._db = new sqlite3.Database(':memory:');

    this._db.serialize();
  }

  query<T = any>(sql: string): Promise<T[]> {
    return new Promise((resolve, reject) => {
      this._db.all(sql, (error: Error | null, rows: T[]) => {
        if (error) {
          return reject(error);
        }

        return resolve(rows);
      });
    })
  }
}

export async function initDatabase() {
  const db = new Database();

  await db.query(`CREATE TABLE IF NOT EXISTS users (
    id INTEGER NOT NULL PRIMARY KEY,
    name STRING NOT NULL,
    email STRING NOT NULL,
    password STRING NOT NULL
  );`);

  return db;
}
import type { Request, Response } from 'express';

export function notFoundMiddleware(req: Request, res: Response) {
  const { path, method } = req;

  res.status(404).json({
    message: 'the request endpoint is not defined',
    code: 'ENDPOINT_NOT_FOUND',
    data: { path, method },
  });
}
import { Router, Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import type { Database } from './db';
import { HttpError } from './error';

interface User {
  id: number;
  name: string;
  email: string;
  password: string;
}

const userSchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
}).required().unknown();

export function createUsersRouter(db: Database) {
  const usersRouter = Router();

  usersRouter.route('/')
    .get(listUsers)
    .post(createUser);

  return usersRouter;

  async function listUsers(req: Request, res: Response, next: NextFunction) {
    try {
      const { query: { name = '' } } = req;

      // NOTE(roman): a sql injection potential
      const sql = `SELECT * FROM users WHERE name LIKE '%${name}%'`;

      const users = await db.query<User>(sql);

      console.log(`listing users whose names contain '${ name }'`);

      res.json(users);

      return;
    } catch (error) {
      return next(error);
    }
  }

  async function createUser(req: Request, res: Response, next: NextFunction) {
    try {
      const { body } = req;

      const { error: validationError } = userSchema.validate(body);
      if (validationError) {
        const { message, details } = validationError;

        return next(new HttpError({ message, httpCode: 400, data: { details } }));
      }

      const { name, email, password } = body;

      // NOTE(roman): not a sql injection potential (probably)
      const [existingUser] = await db.query<User>(`SELECT * FROM users WHERE email = '${email}'`);
      if (existingUser) {
        return next(new HttpError({ message: 'EMAIL_ALREADY_USED', httpCode: 400, data: { email } }));
      }

      // NOTE(roman): another sql injection potential thanks to either `name` or `password`
      await db.query<User>(`
        INSERT INTO users (name, email, password) VALUES
        ('${name}', '${email}', '${password}')
      `);

      // NOTE(roman): not a sql injection potential (probably)
      const [newUser] = await db.query<User>(`SELECT * FROM users WHERE email = '${email}'`);
      if (!newUser || newUser.name !== name || newUser.password !== password) {
        return next(new HttpError({ message: 'congrats, you found the sql injection vulnerability' }))
      }

      res.json(newUser);

      return;
    } catch (error) {
      return next(error);
    }
  }
}
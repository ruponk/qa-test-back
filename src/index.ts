import bodyParser from 'body-parser';
import { join, resolve } from 'path';
import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
 
import { errorMiddleware } from './error';
import { notFoundMiddleware } from './not-found';
import { createUsersRouter } from './users.router';
import { initDatabase } from './db';

const PORT = parseInt(process.env.PORT || '8080');

async function main() {
  const db = await initDatabase();

  const app = express();

  app.use(cors());
  app.use(helmet());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use('/api/v1/users', createUsersRouter(db));

  app.use('/:filename.:extension', (req, res) => {
    const { filename, extension } = req.params;

    res.sendFile(resolve(join('public', `${ filename }.${ extension }`)));
  });

  app.use('/', (req, res) => {
    res.sendFile(resolve(join('public', 'index.html')));
  });

  app.use(notFoundMiddleware);
  app.use(errorMiddleware);

  app.listen(PORT, '0.0.0.0', () => {
    console.log(`server listening on port ${ PORT }`);
  });
}

void main()
  .then(() => console.info('server creation succeeded'))
  .catch(error => console.error('server creation failed', error));